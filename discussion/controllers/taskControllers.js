// containing the functions/statements/logic to be executed once a route has been triggered/entered
// responsible for CRUD operations/methods

// to give access to the contents the for tasks.js file in the models folder; meaning it can use the Task model
const Task = require("../models/task.js")

// create controller functions that responds to the routes
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	// saving mechanisms
	/*
		.then accepts 2 parameters
			-first parameter stores the result object; if we have  successfully saved the object
			-second parameter stores the error object; should there be one
	*/
	return newTask.save().then((savedTask, error) => {
		if(error){
			console.log(error);
			return false
		}else{
			return savedTask
		}
	})
}


/*
	1. look for the task with the corresponding id provided in the URL
	2. delete the task
*/
module.exports.deleteTask = (taskId) => {
	// findByIdAndRemove - finds the items and removes it from the database; it uses id in looking for document
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if (error){
			console.log(error);
			return false
		}else{
			return removedTask
		}
	})
}

module.exports.updateTask = (taskId, requestBody) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return false
		}else{
			result.name = requestBody.name;
			return result.save().then((updateTask, error) =>{
				if(error){
					console.log(error)
					return false
				}else{
					return updateTask
				}
			})
		}
	})
}

/*
module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
	return result
	})
}

This will work but have to be prepeared for error catching!
SO here's the better way..
*/

// retrieving error handling
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return result;
		}
	})
}

module.exports.updateStatus = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		result.status = newStatus.statusCode
		return result.save().then((updatedStatus, error) => {
			if(error){
				console.log(error);
				return false;
			}else{
				return updatedStatus
			}
		})
	})
}