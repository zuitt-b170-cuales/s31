const express = require("express");
const mongoose = require("mongoose");
// this is to load 
const taskRoute = require("./routes/taskRoutes.js")
const app = express()
const port = 3000

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect("mongodb+srv://owencuales:OplanSagittarius1972@wdc028-course-booking.3hxrc.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
	})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

// Schema - What you want to see
// Route - When you want to see
// Controllers - How you want to see

// gives the app an access to the routes 	
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port 3000`))
