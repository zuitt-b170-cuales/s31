// routes - contains all the end points for the application; instead of putting a lot of routes in the eindex.js, we separate them in the other file, routes.js (databaseRoutes.js i.e. taskRoutes.js)

const express = require("express")

// creates a router instance that functions as a middleware and routing system; allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// allows us to use the controllers' functions
const taskController = require("../controllers/taskControllers.js")

// before "/" means accessing localhost:3000/
// through the use of app.use("/tasks", taskRoute) in index.js for the taskRoutes, localhost:3000/tasks/
// accessing different schema, assuming we have others would set up another app.use in the index.js for us and other files inside models, routes, and controllers 
// for example, if you add another schema that is Users, the base uri for that would be set by app.use("/users", userRoutes) and accessing its "/" url inside userRoutes would be localhost:3000/users/

// route to get all the tasks
router.get("/", (req, res) => {
	// taskController - a controller file that is existing in the repo
	// getAllTasks() - a function that is existing or encoded inside the taskController file
	// .then() - waits for the getAllTasks() function to be executed before rpoceeding to the statements (res.send)
	taskController.getAllTasks().then(result => res.send(result));
})

// route for creating a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

// : (colon) - wildcard; if tagged with a parameter, the app will automatically look for the parameter that is in the url
// localhost:3000/tasks/cooking-a-meal
router.delete("/:id", (req, res) => {
	// URL parameter values are accessed through the request object's "params" property; the specified parameter/property of the object will match the URL parameter endpoint
	taskController.deleteTask(req.params.id).then(result => res.send(result))
})

router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

// ************************************************** //

router.get("/:id/complete", (req, res)=>{
	taskController.getTask(req.params.id, req.body).then(result => res.send(result));
})

router.put("/:id/complete", (req, res)=>{
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})

// ************************************************** //

// module.exports - a way for js to treat the value/file as a package that can be impoted/used by other files
module.exports = router;
